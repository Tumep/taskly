import React from 'react';
import { Alert, StyleSheet, Text, View, TextInput, Button } from 'react-native';
import DatePicker from 'react-native-datepicker'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {task: '', currentDate: ''};
  }

  _onPressButton() {
    Alert.alert('You tapped the button!');
  }
  render() {
    return (
      <View style={styles.container}>
      <View style={styles.text}>
        <TextInput></TextInput>
        </View>
        <View style={styles.item}>
        <DatePicker format="DD.MM.YYYY" date={this.state.currentDate} onDateChange={(currentDate) => {this.setState({currentDate: currentDate})}}></DatePicker>
        </View>
        <View style={styles.item}>
        <Button
            onPress={this._onPressButton}
            title="Add"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    margin: 8,
    height: 50,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  item: {
    flex: 1
  },
  text: {
    flex: 2
  }
});
